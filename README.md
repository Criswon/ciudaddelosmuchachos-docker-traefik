## Despliegue de Traefik y sistema de monitorización con Docker

En esta charla se va a enseñar a desplegar de una manera sencilla mediante Docker una infraestructura la cual está funcionando a través del balanceador Traefik. 

Aparte de esto y como complemento, se va a desplegar en un docker-compose una infraestructura de ejemplo que nos va a permitir monitorizar toda nuestra infraestructura basada en Docker.

---

## Resumen de servicios

1. **traefik** Balanceador orientado a microservicios que permite de una manera muy cómoda, sin reinicios y de manera totalmente dinámica crear multiples reglas para gestionar los servicios que se encuentran por debajo de este.
https://traefik.io/
2. **exampleweb** Contiene un apache con php7, el cuál nos va a permitir ver como traefik balancea correctamente todas las peticiones mostrando diferentes hostname en cada petición al balanceador.
3. **portainer** es una interfaz web que nos va a permitir visualizar y configurar Docker. Si nunca has utilizado Docker, se recomiendo para empezar a realizar pruebas con él.
https://www.portainer.io/
4. **monitoring** Diferentes servicios que nos van a permitir ver de una manera sencilla los recursos que son consumidos tanto por la máquina host y sus respectivos contenedores. Los servicios que se han utilizado aquí son Prometheus, CAdvisor, Grafana.
https://prometheus.io/
https://github.com/google/cadvisor
https://grafana.com/
